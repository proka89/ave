module.exports = function (grunt) {
	'use strict';

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		sass: {
			dist: {
				files: {
					'wp-content/themes/<%= pkg.name %>/style.css' : 'wp-content/themes/<%= pkg.name %>/src/scss/styles.scss'
				},
				options: {
					sourceMap: false,
					style: 'compressed'
				}
			}
		},

		postcss: {
			options: {
                map: false,
                processors: [
                    require('autoprefixer')({overrideBrowserslist: 'last 2 versions'})
                ]
            },
			dist: {
				src: 'wp-content/themes/<%= pkg.name %>/style.css'
			}
		},

		watch: {
			files: ['wp-content/themes/<%= pkg.name %>/src/**/*.scss'],
			tasks: ['sass', 'postcss'],
			reload: {
				files: ['wp-content/themes/<%= pkg.name %>/style.css']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['watch']);
};
