<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package ave
 */

?>

	</div><!-- .col-full -->
</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="col-full">
			<div class="footer-wrapper">
				<section class="footer-list-wrapper">
					<div class="footer-list">
						<p class="footer-head">Information</p>
						<?php
							wp_nav_menu(
								array(
									'theme_location'  => 'information',
									'container_class' => 'footer-nav',
								)
							);
						?>
					</div>
					<div class="footer-list">
						<p class="footer-head">Why buy from us</p>
						<?php
							wp_nav_menu(
								array(
									'theme_location'  => 'why_buy_from_us',
									'container_class' => 'footer-nav',
								)
							);
						?>
					</div>
					<div class="footer-list">
						<p class="footer-head">Your account</p>
						<?php
							wp_nav_menu(
								array(
									'theme_location'  => 'your_account',
									'container_class' => 'footer-nav',
								)
							);
						?>
					</div>
					<div class="footer-list">
						<p class="footer-head">Lookbook</p>
						<?php
							wp_nav_menu(
								array(
									'theme_location'  => 'lookbook',
									'container_class' => 'footer-nav',
								)
							);
						?>
					</div>
					<div class="footer-list">
						<?php
						$address = get_field( 'address', 'option' );
						$telephone = get_field( 'telephone', 'option' );
						$email = get_field( 'email_address', 'option' );
						?>
						<p class="footer-head">Contact details</p>
						<?php if ( $address ): ?>
							<address><?php echo nl2br($address) ?></address>
						<?php endif ?>
						<p class="telephone">Telephone: <a href="tel:<?php echo $telephone ?>"><?php echo $telephone ?></a></p>
						<p class="email">Email: <a href="<?php echo $email ?>"><?php echo $email ?></a></p>
					</div>
				</section>

				<section class="footer-bottom-wraper">
					<?php
					$text_top = get_field( 'banner_text_top', 'option' );
					$text_bottom = get_field( 'banner_text_bottom', 'option' );
					?>
					<div class="footer-banner">
						<?php if ( $text_top): ?>
							<p><?php echo $text_top ?></p>
						<?php endif ?>
						<?php if ( $text_bottom): ?>
							<p class="award"> <?php echo $text_bottom ?></p>
						<?php endif ?>
					</div>
					<div class="footer-social ">
						<?php if ( have_rows( 'social_icons', 'option' ) ) : ?>
							<ul class="social-media">
								<?php
								while ( have_rows( 'social_icons', 'option' ) ) :
									the_row();
									$soc_img = get_sub_field( 'social_image', 'option' );
									$soc_link = get_sub_field( 'social_link', 'option' );
								?>
								<li>
									<a href="<?php echo esc_url( $soc_link ); ?>">
										<span class="social-icon" style="background-image: url(<?php echo esc_url( $soc_img['url'] ); ?>);"></span>
									</a>
								</li>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>
				</section>
			</div>
			<?php
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit         - 20
			 */
			do_action( 'storefront_footer' );
			?>

		</div><!-- .col-full -->
		<div class="site-info">
				<div class="footer-site-info">
					<div class="copyright">
						<?php echo esc_html( apply_filters( 'storefront_copyright_text', $content = '&copy; ' . get_bloginfo( 'name' ) . ' ' . date( 'Y' ) ) ); ?>
					</div>
					<div class="design">
						<p>Designed by: <?php the_field( 'design_by', 'option' ); ?></p>
						<p>Developed by: <?php the_field( 'developed_by', 'option' ); ?></p>
					</div>
				</div>
			</div><!-- .site-info -->
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
