<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package Ave
 * @since Ave 1.0.0
 */

/**
 * Load theme styles and js.
 */

function include_slick_slider_min_js() {
    wp_enqueue_script( 'slick_slider_min_js', get_stylesheet_directory_uri() . '/src/js/slick.min.js', array( 'jquery' ) );
};

add_action( 'wp_enqueue_scripts', 'include_slick_slider_min_js' );

