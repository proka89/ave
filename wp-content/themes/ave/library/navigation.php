<?php
/**
 * Register Menus
 *
 * @link http://codex.wordpress.org/Function_Reference/register_nav_menus#Examples
 * @package ave
 */

register_nav_menus(
	array(
		'why_buy_from_us'       => esc_html__( 'Why buy from us', 'ave' ),
		'information'        	=> esc_html__( 'Information', 'ave' ),
		'lookbook'         		=> esc_html__( 'Lookbook', 'ave' ),
		'your_account'         	=> esc_html__( 'Your account', 'ave' ),
		'header_top'         	=> esc_html__( 'Header top', 'ave' ),
	)
);
