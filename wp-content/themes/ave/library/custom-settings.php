<?php
/**
 * Theme custom settings
 *
 * @package ave
 */

function ave_update_header() {
	remove_action( 'storefront_header', 'storefront_site_branding', 20 );
	add_action( 'storefront_header', 'storefront_site_branding', 50 );

	remove_action( 'storefront_header', 'storefront_primary_navigation', 50 );
	add_action( 'storefront_header', 'storefront_primary_navigation', 50 );

	remove_action( 'storefront_header', 'storefront_product_search', 40 );
	add_action( 'storefront_header', 'storefront_product_search', 50 );

	remove_action( 'storefront_header', 'storefront_header_cart', 60 );
	add_action( 'storefront_header', 'storefront_header_cart', 0 );
}

add_action( 'init', 'ave_update_header' );

/**
 * Register ACF subpages
 */
if ( function_exists( 'acf_add_options_sub_page' ) ) {
	acf_add_options_sub_page(
		array(
			'page_title' => 'Global Settings',
			'menu_title' => 'Global',
		)
	);

	acf_add_options_sub_page(
		array(
			'page_title' => 'Footer Content',
			'menu_title' => 'Footer',
		)
	);

	acf_add_options_sub_page(
		array(
			'page_title' => 'Header Content',
			'menu_title' => 'Header',
		)
	);

	acf_add_options_sub_page(
		array(
			'page_title' => 'Important messages',
			'menu_title' => 'Messages',
		)
	);
}

/**
 * Remove storefront footer widgets
 */
function ave_update_footer() {
	remove_action( 'storefront_footer', 'storefront_footer_widgets', 10 );
}

add_action( 'init', 'ave_update_footer' );

/**
 * Add storefront credit function
 */
function storefront_credit() {
	$links_output = '';

	$links_output = apply_filters( 'storefront_credit_links_output', $links_output );
	?>
	<?php
}
