
<?php
/**
 * Author: Srdjan Margetic
 *
 * Ave functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package ave
 */

/** Enqueue scripts */
require_once 'library/enqueue-scripts.php';
/** Custom settings */
require_once 'library/custom-settings.php';
/** Navigation  */
require_once 'library/navigation.php';

