# Change Log
All notable changes to this project will be documented in this file.

## [Unreleased]
### Changed
- Update git ignore file

### Added
- jshint
- sass-lint.yml
- WordPress
- Storefront theme
- WooCommerce
- Basic base dump
- Add Advanced Custom Fields plugin
- Add Disable Gutenberg plugin
- Add Storefront Visual Hook Guide plugin
- Add Simply show hooks plugin
- Add package.json file
- Add Gruntfile.js file
- Add package-lock.json file
- Add child-theme
- Add slick.min.js file
- Add enqueue-scripts.php file
- Add slick.scss file
- Add editorconfig file
- Add Fonts
- Add navigation.php file
- Add styles for the footer
- Add folder for ACF

### Removed
- Remove Twenty Nineteen theme
- Remove Twenty Seventeen theme
